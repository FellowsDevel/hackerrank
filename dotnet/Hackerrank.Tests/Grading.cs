using System;
using Xunit;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Hackerrank.Tests
{
    public class GradingTest
    {

        /*
         * Complete the gradingStudents function below.
         */
        private int[] gradingStudents(int[] grades)
        {
            int[] resp = new int[grades.Length];
            for (int x = 0; x < grades.Length; x++)
            {
                int nextMultiple = grades[x];
                while (nextMultiple % 5 != 0)
                {
                    nextMultiple++;
                }
                if (grades[x] >= 38 && (nextMultiple - grades[x]) < 3)
                {
                    resp[x] = nextMultiple;
                }
                else
                {
                    resp[x] = grades[x];
                }
            }
            return resp;
        }

        [Theory]
        [InlineData(new int[] { 73, 67, 38, 33 })]
        public void Grading_Student_Test(int[] numbers)
        {
            Assert.Equal(new int[] { 75, 67, 40, 33 }, gradingStudents(numbers));
        }
    }
}
