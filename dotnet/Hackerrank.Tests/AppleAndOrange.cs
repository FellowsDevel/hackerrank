using System;
using Xunit;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Hackerrank.Tests
{
    public class AppleAndOrangeTest
    {

        // Complete the countApplesAndOranges function below.
        private int[] countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges)
        {
            int[] resp = new int[] { 0, 0 };

            for (int x = 0; x < apples.Length; x++)
            {
                int apple = a + apples[x];
                if (apple >= s && apple <= t)
                {
                    resp[0]++;
                }
            }

            for (int x = 0; x < oranges.Length; x++)
            {
                int orange = b + oranges[x];
                if (orange >= s && orange <= t)
                {
                    resp[1]++;
                }
            }

            return resp;
        }

        [Theory]
        [InlineData(7, 11, 5, 15, new int[] { -2, 2, 1 }, new int[] { 5, -6 })]
        public void Grading_Student_Test(int s, int t, int a, int b, int[] apples, int[] oranges)
        {
            Assert.Equal(new int[] { 1, 1 }, countApplesAndOranges(s, t, a, b, apples, oranges));
        }
    }
}
