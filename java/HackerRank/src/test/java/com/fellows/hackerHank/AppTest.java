package com.fellows.hackerHank;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.junit.Test;

import com.fellows.hackerHank.JavaAnagram.Anagram;

public class AppTest {

	public AppTest() {
	}

	@Test
	public void out01() {
		String s = "welcometojava";
		int k = 3;
		String actual = App.getSmallestAndLargest(s, k);
		assertEquals("ava\nwel", actual);
	}

	@Test
	public void plaindrome() {
		String s = "madam";
		String actual = App.isPalindrome(s);
		assertEquals("Yes", actual);
	}

	@Test
	public void notPlaindrome() {
		String s = "medam";
		String actual = App.isPalindrome(s);
		assertEquals("No", actual);
	}

	@Test
	public void isAnagram() {
		String a = "anagram";
		String b = "margana";
		boolean actual = Anagram.isAnagram(a, b);
		assertTrue(actual);

		a = "anagramm";
		b = "marganaa";
		actual = Anagram.isAnagram(a, b);
		assertFalse(actual);
	}

	@Test
	public void stringToken() {

		String originalMessage = "He is a very very good boy, isn't he?";
		String[] response = { "He", "is", "a", "very", "very", "good", "boy", "isn", "t", "he" };

		originalMessage = originalMessage.replaceAll("  ", " ");
		originalMessage = originalMessage.replaceAll("[!'\"@#$%]", " ");
		originalMessage = originalMessage.replaceAll("[^A-Za-z ]", "");
		originalMessage = originalMessage.replaceAll("  ", " ");

		assertArrayEquals(response, originalMessage.split(" "));
	}

	@Test
	public void stringToken2() {
		// String s = " YES leading spaces are valid, problemsetters are evillllll";
		String s = "                        ";
		// Write your code here.
		s = s.trim().replaceAll("[ ]{2,}", " ");
		s = s.replaceAll("[!'\"@#$%\\n\\r]", " ");
		s = s.replaceAll("[^A-Za-z ]", "");
		s = s.replaceAll("[ ]{2,}", " ");

		String[] arr = !s.isEmpty() ? s.split(" ") : null;
		int len = arr != null ? arr.length : 0;
		System.out.println(len);
		for (int x = 0; x < len; x++) {
			System.out.println(arr[x]);
		}
	}

	@Test
	public void stringToken3() {
		String s = getString();

		String[] resp = { "Good", "luck", "with", "this", "case", "h", "jrkjc", "c", "l", "m", "e", "i", "vct", "h",
				"ss", "pqk", "v", "i", "olf", "tuoej", "p", "r", "jrpjpuo", "a", "udc", "mu", "tk", "g", "dc", "j", "o",
				"mui", "brljn", "jv", "p", "rsklqu", "p", "a", "lv", "l", "n", "dl", "quo", "cml", "pld", "qf", "l",
				"s", "t", "nb", "ud", "j", "etc", "q", "a", "j", "f", "ugc", "eer", "c", "ci", "de", "lm", "p", "iwk",
				"nwa", "e", "su", "s", "u", "ga", "l", "w", "xlkod", "f", "e", "v", "oo", "ukaa", "v", "t", "xe", "j",
				"cl", "vmh", "hi", "tl", "xa", "aw", "ugeibo", "c", "r", "oo", "v", "qte", "ri", "elbf", "ibg", "qk",
				"i", "m", "nm", "s", "ainis", "s", "u", "m", "rhd", "fgi", "h", "v", "mji", "tu", "oj", "t", "c", "d",
				"x", "hxtp", "a", "bf", "xj", "l", "j", "ela", "wuj", "is", "pj", "gu", "fs", "e", "o", "v", "i", "s",
				"s", "i", "b", "k", "kab", "tw", "im", "c", "vlud", "k", "ki", "e", "ft", "gpcf", "t", "g", "k", "m",
				"c", "r", "snv", "w", "b", "rw", "hwoh", "dfl", "hn", "u", "cb", "ep", "ucsse", "j", "a", "d", "h", "q",
				"p", "w", "q", "rjp", "tln", "j", "vofvwg", "sj", "rx", "pur", "l", "dx", "vo", "b", "lk", "sljnm", "k",
				"xox", "i", "cv", "va", "l", "du", "toe", "sdwx", "g", "c", "s", "uded", "gw", "od", "tqsv", "v", "t",
				"v", "fnl", "o", "amka", "ll", "x", "s", "q", "a", "frs", "s", "ltubs", "wdjfdrf", "x", "jk", "jgkb",
				"d", "s", "d", "k", "n", "m", "sedwkpe", "ev", "h", "icr", "ggqxvap", "j", "g", "hva", "o", "kja", "q",
				"j", "j", "q", "ra", "iak", "c", "u", "vqnf", "thfw", "wx", "ccn", "mox", "sp", "do", "u", "r", "ei",
				"ifah", "nl", "u", "g", "m", "qipu", "r", "c", "e", "ws", "op", "hng", "fbp", "fr", "vle", "qh", "d",
				"fv", "okcei", "p", "e", "oal", "a", "nsxi", "g", "to", "w", "x", "qfvv", "sr", "cjmo", "kchrkonuc",
				"f", "ush", "wd", "xptas", "rfrt", "o", "llgwf", "jclpl", "wmt", "h", "r", "rv", "o", "lhnxnbf", "h",
				"fqfnla", "pft", "n", "b", "uu", "tbba", "sl", "u", "ij", "f", "i", "jox", "xu", "ext", "p", "gs", "un",
				"dc", "fx", "p", "o", "kh", "rl", "l", "j", "nkgevg", "xdrq", "q", "s", "wv", "umu", "olw", "jaidu",
				"ilpr", "e", "fc", "qcr", "p", "htpn", "g", "xt", "d", "d", "uqk", "cp", "cts", "jkg", "e", "i", "op",
				"j", "utt", "rf", "gvm", "t", "qgjr", "r", "r", "s", "cpc", "m", "n", "a", "wde", "pcg", "v", "e",
				"ioek", "nlc", "fh", "ahk", "ei", "ejmp", "d", "fq", "enmeoesu", "skntwnolsx", "j", "bt", "bgb", "dhtt",
				"r", "jcl", "mdh", "i", "kr", "m", "jcl", "fgix", "gq", "enpuk", "n", "g", "rl", "i", "ei", "xedl", "o",
				"h", "e", "a", "x", "wjp", "uge", "h", "w", "g", "wb", "dg", "wlkxli", "qi", "x", "ff", "eq", "gbe",
				"bakb", "m", "c", "p", "aos", "vv", "c", "m", "labuj", "r", "s", "t", "oign", "xwn", "a", "c", "ffwpic",
				"wck", "gmkahnse", "si", "q", "viev", "a", "ndfg", "f", "p", "utcumoxb", "kgd", "m", "n", "g", "gnk",
				"b", "i", "rr", "x", "x", "uxx", "g", "p", "af", "s", "piu", "hgs", "o", "kpmrltmrqe", "s", "m",
				"mbjfnxq", "p", "g", "f", "i", "v", "l", "wc", "u", "ij" };

		String[] arr = !s.isEmpty() ? s.split(" ") : null;

        assert arr != null;
        int max = Math.min(arr.length, resp.length);
		for (int x = 0; x < max; x++) {
			if (!arr[x].equals(resp[x])) {
				System.out.print("BAD ->> ");
			}
			System.out.println(x + " - " + arr[x] + " - " + resp[x]);
		}
	}

	private static String getString() {
		String s = "Good luck with this case!!! h jrkjc c l m e i vct h ss pqk_v i olf tuoej_          p r jrpjpuo a.          udc mu tk g dc,          j o mui brljn!jv p rsklqu p?a lv l n dl quo!cml pld qf l s!          t nb ud j etc q           a j f ugc eer c,          ci de lm p iwk_nwa e su s u ga.l w xlkod f e v_          oo ukaa v t xe.          j cl vmh hi tl!          xa aw ugeibo?c r oo v qte ri,          elbf ibg qk i_m nm s ainis s           u m rhd fgi h v!          mji tu oj t c d.x hxtp a bf xj.          l j ela wuj is           pj gu fs e o v i,s s i b k kab tw@          im c vlud k ki!          e ft gpcf t g k'          m c r snv w b rw,hwoh dfl hn u@cb ep ucsse j_          a d h q p w q rjp_          tln j vofvwg_sj rx pur l dx_          vo b lk sljnm?          k xox i cv va l,          du toe sdwx g@c s uded gw od!          tqsv v t v fnl'          o amka ll x s q_          a frs s ltubs?wdjfdrf x jk.jgkb d s d k n m@sedwkpe ev h_icr ggqxvap@          j g hva o kja q?          j j q ra iak c u!          vqnf thfw wx!          ccn mox sp do'u r ei ifah nl'u g m qipu r c e           ws op hng fbp.          fr vle qh d fv_okcei p e oal_a nsxi g to w x!          qfvv sr cjmo_          kchrkonuc f'ush wd xptas!rfrt o llgwf?          jclpl wmt h r           rv o lhnxnbf           h fqfnla pft?n b uu tbba sl_u ij f i jox xu.ext p gs un dc           fx p o kh rl l j'nkgevg xdrq?          q s wv umu olw?jaidu ilpr e?fc qcr p htpn           g xt d d uqk cp,          cts jkg e i op_j utt rf gvm t!          qgjr r r s cpc,          m n a wde pcg v?e ioek nlc fh@ahk ei ejmp d_fq enmeoesu?          skntwnolsx!j bt bgb dhtt!          r jcl mdh i kr,          m jcl fgix gq_enpuk n g rl i?ei xedl o h e a!x wjp uge h w g'wb dg wlkxli qi x ff eq gbe_bakb m c p aos_vv c m labuj r!          s t oign xwn a@c ffwpic wck_          gmkahnse si!q viev a ndfg,f p utcumoxb'          kgd m n g gnk b!          i rr x x uxx g p_          af s piu hgs o?          kpmrltmrqe@s m mbjfnxq p'          g f i v l wc u ij@";
		// Write your code here.
		s = s.trim().replaceAll("[ ]{2,}", " ").replaceAll("[.,?!_'\"@#$%\\n\\r]", " ").replaceAll("[^A-Za-z ]", "")
				.replaceAll("[ ]{2,}", " ");
		return s;
	}

	@Test
	public void javaRegex2DuplicateWords() {

		final String regex = "\\b(\\w+)(\\b\\W+\\b\\1\\b)*";
		final Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);

		final String[] inputStrings = { "Goodbye bye bye world world world", "Sam went went to to to his business",
				"Reya is is the the best player in eye eye game", "in inthe", "Hello hello Ab aB" };

		final String[] resultString = { "Goodbye bye world", "Sam went to his business",
				"Reya is the best player in eye game", "in inthe", "Hello Ab" };

		for (int x = 0; x < inputStrings.length; x++) {
			final Matcher m = p.matcher(inputStrings[x]);
			while (m.find()) {
				inputStrings[x] = inputStrings[x].replaceAll(m.group(0), m.group(1));
			}
			assertEquals(resultString[x], inputStrings[x]);
		}
	}

	@Test
	public void kangaroo() {
		assertEquals("YES", App.kangaroo(0, 3, 4, 2));
		assertEquals("NO", App.kangaroo(0, 2, 5, 3));
		assertEquals("NO", App.kangaroo(21, 6, 47, 3));
		assertEquals("YES", App.kangaroo(4523, 8092, 9419, 8076));
	}

	// Java Primality Test
	@Test
	public void isPrime() {
		String n = "37";
		if (new BigInteger(n).isProbablePrime(1)) {
			System.out.println("prime");
		} else {
			System.out.println("not prime");
		}
	}

	@Test
	public void tagContentExtractor() {
		assertTrue(testa("<h1>teste</h1>"));
		assertTrue(testa("<h1><h1>testeoks</h1></h1><par>pode ser também</par>"));
		assertFalse(testa("<Amee>aqui nao funciona</amee>"));
		assertTrue(testa("<Sa premium>aqui funciona</Sa premium>"));
		assertFalse(testa("<h1><Amee>aqui nao funciona</amee>teste</h1>"));
	}

	private boolean testa(String linha) {
		boolean found = false;
		Pattern p = Pattern.compile("<(.+)>([^<]+)</\\1>");
		Matcher m = p.matcher(linha);
		while (m.find()) {
			System.out.println(m.group(2));
			found = true;
		}
		if (!found) {
			System.out.println("none");
			return false;
		}
		return true;
	}

	@Test
	public void javaStack() {
		var myMap = new HashMap<String, String>();
		myMap.put("(", ")");
		myMap.put("[", "]");
		myMap.put("{", "}");

		var myStack = new Stack<String>();
		String input = "{[]}";
		String last;
		String next;
		for (int i = 0; i < input.length(); i++) {
			next = Character.toString(input.charAt(i));
			if (myStack.empty()) {
				myStack.push(next);
			} else {
				last = myStack.peek();
				if (myMap.containsKey(last)) {
					if (myMap.get(last).equals(next)) {
						myStack.pop();
					} else {
						myStack.push(next);
					}
				} else {
					myStack.push(next);
					break;
				}
			}
		}
		System.out.println(myStack.empty());

	}

	@Test
	public void hashSet() {
		
		int t = 5;
        String [] pair_left = new String[t];
        String [] pair_right = new String[t];
        
        pair_left[0] = "john";
        pair_right[0] = "tom";
        pair_left[1] = "john";
        pair_right[1] = "mary";
        pair_left[2] = "john";
        pair_right[2] = "tom";
        pair_left[3] = "mary";
        pair_right[3] = "anna";
        pair_left[4] = "mary";
        pair_right[4] = "anna";

        Set<String> list = new HashSet<String>();

        for (int i = 0; i < t; i++) {
            list.add(String.join("", pair_left[i]) + " " + String.join("", pair_right[i]));
            System.out.println(list.size());
        }		
	}
}
