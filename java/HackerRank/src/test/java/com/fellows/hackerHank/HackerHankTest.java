package com.fellows.hackerHank;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class HackerHankTest {

    private static Stream<Arguments> provideStringsForIsBlank() {
        return Stream.of(
                Arguments.of(null, true),
                Arguments.of("", true),
                Arguments.of("  ", true),
                Arguments.of("not blank", false)
        );
    }

    @Test
    @ParameterizedTest
    @MethodSource("provideStringsForIsBlank")
    public void validTests() {
        String linha = "000.12.234.23.23";
        Pattern p = Pattern.compile("(\\d+)\\.(\\d+)\\.(\\d+)\\.(\\d+)");
        Matcher op = p.matcher(linha);
        while (op.find()) {
            if (
                    (Integer.parseInt(op.group(1)) <= 255) &&
                            (Integer.parseInt(op.group(2)) <= 255) &&
                            (Integer.parseInt(op.group(3)) <= 255) &&
                            (Integer.parseInt(op.group(4)) <= 255)) {
                System.out.println("true");
            } else {
                System.out.println("false");
            }
        }

    }

    @Test
    public void isPrime() throws IOException {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            String n = bufferedReader.readLine();
            BigInteger numb = new BigInteger(n);
            if (numb.isProbablePrime(1)) {
                System.out.println("prime");
            } else {
                System.out.println("not prime");
            }
        }
    }



}
