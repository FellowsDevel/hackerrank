package com.fellows.hackerHank;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IpAddressTest {

    @Test
    public void validTests() {
//        String linha = "000.12.12.034";
        String linha = "000.12.234.23.23";
//        Pattern p = Pattern.compile("(\\d+).(\\d+).(\\d+).(\\d+)");
        Pattern p = Pattern.compile("(\\d+)\\.(\\d+)\\.(\\d+)\\.(\\d+)");
        Matcher op = p.matcher(linha);
        while (op.find()) {
            if (
                    (Integer.parseInt(op.group(1)) <= 255) &&
                            (Integer.parseInt(op.group(2)) <= 255) &&
                            (Integer.parseInt(op.group(3)) <= 255) &&
                            (Integer.parseInt(op.group(4)) <= 255)) {
                System.out.println("true");
            } else {
                System.out.println("false");
            }
        }

    }


}
