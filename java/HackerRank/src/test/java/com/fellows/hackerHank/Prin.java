package com.fellows.hackerHank;

import java.io.*;

public class Prin {
    public static void main(String[] args) {


        String filePath = "students.ser";
        Classe2 student = new Classe2("John", 22);

        try (FileOutputStream fos = new FileOutputStream(filePath);
             ObjectOutputStream outputStream = new ObjectOutputStream(fos)) {

            outputStream.writeObject(student);

        } catch (IOException ex) {
            System.err.println("IO error: " + ex);
        }

        try (FileInputStream fis = new FileInputStream(filePath);
             ObjectInputStream inputStream = new ObjectInputStream(fis)) {

            Classe2 student2 = (Classe2) inputStream.readObject();
            System.out.println(student2);
        } catch (ClassNotFoundException ex) {
            System.err.println("Class not found: " + ex);
        } catch (IOException ex) {
            System.err.println("IO error: " + ex);
        }

    }


    public static class Classe1 implements Serializable{
        @Serial
        private static final long serialVersionUID = 12234L;
        private String nome;

        public Classe1(String nome) {
            this.nome = nome;
            System.out.println("Criando Classe1");
        }

        public String getNome() {
            return nome;
        }

        public void setNome(String nome) {
            this.nome = nome;
        }
    }

    public static class Classe2 extends Classe1 implements Serializable {
        @Serial
        private static final long serialVersionUID = 1234L;

        private Integer idade;

        public Classe2(String nome, Integer idade) {
            super(nome);
            this.idade = idade;
            System.out.println("Criando Classe2");
        }

        public Integer getIdade() {
            return idade;
        }

        public void setIdade(Integer idade) {
            this.idade = idade;
        }

        @Override
        public String toString() {
            return "Classe2{" +
                    "idade=" + idade +
                    ", nome='" + getNome() + '\'' +
                    '}';
        }
    }
}
