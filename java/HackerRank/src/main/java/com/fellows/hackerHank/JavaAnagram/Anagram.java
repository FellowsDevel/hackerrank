package com.fellows.hackerHank.JavaAnagram;

/**
 * Anagram
 */
public class Anagram {

   private static String sort(char[] arr) {
      int size = arr.length;

      for (int i = 0; i < size - 1; i++) {
         for (int j = i + 1; j < arr.length; j++) {
            if (String.valueOf(arr[i]).compareToIgnoreCase(String.valueOf(arr[j])) > 0) {
               String temp = String.valueOf(arr[i]);
               arr[i] = arr[j];
               arr[j] = temp.toCharArray()[0];
            }
         }
      }
      return new String(arr);
   }

   public static boolean isAnagram(String a, String b) {
      char[] achars = a.toCharArray();
      char[] bchars = b.toCharArray();

      return sort(achars).equalsIgnoreCase(sort(bchars));
   }

}