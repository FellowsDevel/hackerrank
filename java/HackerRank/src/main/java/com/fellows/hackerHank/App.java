/*----------------------------------------------------------------------------------------
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License. See LICENSE in the project root for license information.
 *---------------------------------------------------------------------------------------*/

package com.fellows.hackerHank;

import java.util.SortedSet;
import java.util.TreeSet;

public class App {

    public static String getSmallestAndLargest(String s, int k) {
        String smallest = "";
        String largest = "";

        // Complete the function
        // 'smallest' must be the lexicographically smallest substring of length 'k'
        // 'largest' must be the lexicographically largest substring of length 'k'

        long start = System.nanoTime();
        SortedSet<String> sets = new TreeSet<String>();
        for (int i = 0; i <= s.length() - k; i++) {
            sets.add(s.substring(i, i + k));
        }
        smallest = sets.first();
        largest = sets.last();
        long end = System.nanoTime();

        System.out.println("Start     : " + start);
        System.out.println("End       : " + end);
        System.out.println("Difference: " + (end - start) + "\n");

        start = System.nanoTime();

        for (int x = 0; x <= s.length() - k; x++) {
            String part = s.substring(x, x + k);
            if (smallest.isEmpty()) {
                smallest = part;
            }
            if (largest.isEmpty()) {
                largest = part;
            }

            if (largest.compareTo(part) < 0) {
                largest = part;
            }
            if (smallest.compareTo(part) > 0) {
                smallest = part;
            }
        }

        end = System.nanoTime();

        System.out.println("Start     : " + start);
        System.out.println("End       : " + end);
        System.out.println("Difference: " + (end - start) + "\n");

        return smallest + "\n" + largest;
    }

    public static void main(String[] args) {
        String s = "welcometojava";
        int k = 3;
        System.out.println(getSmallestAndLargest(s, k));
    }

    public static String isPalindrome(String word) {
        String resp = "No";
        StringBuilder reverse = new StringBuilder();
        for (int x = word.length() - 1; x >= 0; x--) {
            reverse.append(word.charAt(x));
        }
        if (reverse.toString().equals(word)) {
            resp = "Yes";
        }
        return resp;
    }

    public static String kangaroo(int x1, int v1, int x2, int v2) {
        if ((x2 > x1 && v2 >= v1) || ((x1 - x2) % (v2 - v1)) != 0) {
            return "NO";
        } else {
            return "YES";
        }
    }
}